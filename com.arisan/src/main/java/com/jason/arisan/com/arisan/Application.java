package com.jason.arisan.com.arisan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.ArrayList;
import java.util.Scanner;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Arisan Randomizer
 * @author NEXSOFT
 * @version 1.0
 * @since 21/10/2021
 */

@SpringBootApplication
public class Application {
	static ArrayList<Person> personList = new ArrayList<Person>();
	static ArrayList<String> ticketPool = new ArrayList<String>();
	static int totalMoney = 0;
	
	public static void main(String[] args) {
		int personCount = 0;
		int moneyCount = 0;
		String name;
		String ticket;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("=====================================");
		System.out.println("Arisan Randomizer");
		System.out.println("=====================================");
		
		try{
			System.out.print("Jumlah orang          : ");
			personCount = scanner.nextInt();
			System.out.print("Jumlah uang per orang : ");
			moneyCount = scanner.nextInt();
			scanner.nextLine();
			totalMoney = personCount * moneyCount;
		}catch(Exception e) {
			System.out.println("Invalid Input");
			System.exit(0);
		}
		
		for(int i = 0; i < personCount; i++) {
			System.out.print("Masukkan nama orang ke-" + (i+1) + " : ");
			name = scanner.nextLine();
			name.strip();
			ticket = RandomStringUtils.randomAlphabetic(7);
			personList.add(new Person(name, ticket));
			ticketPool.add(ticket);
		}
		System.out.println("=====================================");
		for(int i = 0; i < personList.size(); i++) {
			System.out.println("Nama peserta      : " + personList.get(i).getName());
			System.out.println("Nomor ticket      :" + personList.get(i).getTicket());
			System.out.println("=====================================");
		}
		while(personList.size() > 0) {
			int randomNumber = ThreadLocalRandom.current().nextInt(0, ticketPool.size());
			System.out.println("Pemenang hari ini : " + ticketPool.get(randomNumber));
			System.out.println(winner(randomNumber) + " mendapatkan " + totalMoney);
			ticketPool.remove(randomNumber);
			personList.remove(randomNumber);
			System.out.println("=====================================");
		}
	}
	
	public static String winner(int randomNumber) {
		return personList.get(randomNumber).getName();
	}
}
