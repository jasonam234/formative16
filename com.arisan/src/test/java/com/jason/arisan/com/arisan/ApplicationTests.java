package com.jason.arisan.com.arisan;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ApplicationTests {	
	@Test
	void trueWinner() {
		Application.personList.add(new Person("Person a", "aaa"));
		Application.personList.add(new Person("Person b", "bbb"));
		Application.personList.add(new Person("Person c", "ccc"));
		
		assertEquals("Person c", Application.winner(2));
	}
	
	@Test
	void outOfBoundWinner() {
		Application.personList.add(new Person("Person a", "aaa"));
		Application.personList.add(new Person("Person b", "bbb"));
		Application.personList.add(new Person("Person c", "ccc"));
		assertThrows(IndexOutOfBoundsException.class, () -> {
			Application.winner(10);
		});
	}

}
