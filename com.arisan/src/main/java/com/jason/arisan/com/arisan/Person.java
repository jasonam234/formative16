package com.jason.arisan.com.arisan;

public class Person {
	private String name;
	private String ticket;
	
	public Person() {
	}
	
	public Person(String newName, String newTicket) {
		this.name = newName;
		this.ticket = newTicket;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getTicket() {
		return this.ticket;
	}
}
